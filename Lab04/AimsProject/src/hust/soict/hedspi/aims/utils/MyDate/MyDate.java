/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.hedspi.aims.utils.MyDate;

import static java.lang.Integer.parseInt;
import java.util.Scanner;

/**
 *
 * @author APC
 */
public class MyDate {
    public int day;
    public int month;
    public int year;
    public String date;
    public MyDate(){
    }
    public MyDate(String day, String month, String year){
        int intMonth, intDay, intYear;
        
        intDay = parseInt(day.substring(0,day.length()-2 ));
        //converse string month to int month
        intMonth = convertStrmonthToIntmonth(month);
        //converse year 
        intYear = parseInt(year);
        //check
        if(checkDay(intDay, intMonth, intYear)) {
            this.day = intDay;
            this.month = intMonth;
            this.year = intYear;
        }
        else System.out.println("Day not exist!");
    }
    
    public void accept(){
        int intMonth, intDay, intYear;
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Input Date: ");
        String inp = keyboard.nextLine();
        String[] splittedStr = inp.split(" ",-2);
        if(validateDay(splittedStr[1])){
            //converse day
            intDay = parseInt(splittedStr[1].substring(0,splittedStr[1].length()-2));
            //converse string month to int month
            intMonth = convertStrmonthToIntmonth(splittedStr[0]);
            //converse year 
            intYear = parseInt(splittedStr[2]);
            System.out.println(""+intDay+intMonth+intYear);
            //check
            if(checkDay(intDay, intMonth, intYear)){
                this.day = intDay;
                this.month = intMonth;
                this.year = intYear;
            }else System.out.println("Day not exist!");;
        }else System.out.println("Invalid Day!");
    }
    public void print(){
        String show="";
        switch(month){
            case 1: show += "January "; break;
            case 2: show += "February "; break;
            case 3: show += "March "; break;
            case 4: show += "April "; break;
            case 5: show += "May "; break;
            case 6: show += "June "; break;
            case 7: show += "July "; break;
            case 8: show += "August "; break;
            case 9: show += "September "; break;
            case 10: show += "October "; break;
            case 11: show += "November "; break;
            case 12: show += "December "; break;
        }
        switch(day){
            case 1: show+=day+"1st ";break;
            case 2: show+=day+"2nd ";break;
            default : show+=day+"th ";break;
        }
        show+=year;
        System.out.println(show);
    }
    private int convertStrmonthToIntmonth(String month){
        if( month.compareTo("January") == 0 || 
            month.compareTo("Jan") == 0 ||
            month.compareTo("Jan.") == 0 ){
            return 1;
        }
        if( month.compareTo("February") == 0 || 
            month.compareTo("Feb") == 0 ||
            month.compareTo("Feb.") == 0 ){
            return 2;
        }
        if( month.compareTo("March") == 0 || 
            month.compareTo("Mar") == 0 ||
            month.compareTo("Mar.") == 0 ){
            return 3;
        }
        if( month.compareTo("April") == 0 || 
            month.compareTo("Apr") == 0 ||
            month.compareTo("Apr.") == 0 ){
            return 4;
        }
        if( month.compareTo("May") == 0 ){
            return 5;
        }
        if( month.compareTo("June") == 0 || 
            month.compareTo("Jun") == 0 ){
            return 6;
        }
        if( month.compareTo("July") == 0 || 
            month.compareTo("Jul") == 0){
            return 7;
        }
        if( month.compareTo("August") == 0 || 
            month.compareTo("Aug") == 0 ||
            month.compareTo("Aug.") == 0 ){
            return 8;
        }
        if( month.compareTo("September") == 0 || 
            month.compareTo("Sep") == 0 ||
            month.compareTo("Sep.") == 0 ){
            return 9;
        }
        if( month.compareTo("October") == 0 || 
            month.compareTo("Oct") == 0 ||
            month.compareTo("Oct.") == 0 ){
            return 10;
        }
        if( month.compareTo("November") == 0 || 
            month.compareTo("Nov") == 0 ||
            month.compareTo("Nov.") == 0 ){
            return 11;
        }
        if( month.compareTo("December") == 0 || 
            month.compareTo("Dec") == 0 ||
            month.compareTo("Dec.") == 0 ){
            return 12;
        }
        return 0;
    }
    private static Boolean checkYear(int year){
        return (((year % 4 == 0) && (year % 100 != 0)) ||
             (year % 400 == 0));
    }
    private static Boolean checkDay(int day, int month, int year){
        switch(month){
            case 1: return day<=31;
            case 2: 
                if(checkYear(year)) return day<=29;
                else return day<=28;
            case 3: return day<=31;
            case 4: return day<=30;
            case 5: return day<=31;
            case 6: return day<=30;
            case 7: return day<=31;
            case 8: return day<=31;
            case 9: return day<=30;
            case 10: return day<=31;
            case 11: return day<=30;
            case 12: return day<=31;
        }
        return false;
    }
    private static boolean validateDay(String day){
        String tail = day.substring(day.length()-2,day.length());
        int dayNum = parseInt(day.substring(0,day.length()-2));
        switch(dayNum){
            case 1: return tail.compareTo("st")==0?true:false;
            case 2: return tail.compareTo("nd")==0?true:false;
            case 3: return tail.compareTo("rd")==0?true:false;
            default: return tail.compareTo("th")==0?true:false;
        }
    }
    public void printByFormat(int num){
        String show1,show2,show3,show4,show5;
        switch(num){
            case 1:
                show1=""+year+"-";
                if(month>=10) show1+=""+month+"-";
                else show1+="0"+month+"-";
                if(day>=10) show1+=day;
                else show1+="0"+day;
                System.out.println(show1); 
                break; //yyyy-MM-dd
            case 2:
                show2=day+"/"+month+"/"+year;
                System.out.println(show2); 
                break; //d/M/yyyy
            case 3:
                if(day>=10) show3=""+day;
                else show3="0"+day;
                switch(month){
                    case 1: show3 += "-Jan-"; break;
                    case 2: show3 += "-Feby-"; break;
                    case 3: show3 += "-Mar-"; break;
                    case 4: show3 += "-Apr-"; break;
                    case 5: show3 += "-May-"; break;
                    case 6: show3 += "-Jun-"; break;
                    case 7: show3 += "-Jul-"; break;
                    case 8: show3 += "-Aug-"; break;
                    case 9: show3 += "-Sep-"; break;
                    case 10: show3 += "-Oct-"; break;
                    case 11: show3 += "-Nov-"; break;
                    case 12: show3 += "-Dec-"; break;
                }
                show3 += year;
                System.out.println(show3); 
                break; //dd-MMM-yyyy
            case 4:
                if(month>=10) show4=""+month+"-";
                else show4="0"+month+"-";
                if(day>=10) show4+=day+"-";
                else show4+="0"+day+"-";
                show4+=year;
                System.out.println(show4); 
                break; //mm-dd-yyyy
            case 5:
                switch(month){
                    case 1: show5 = "Jan"; break;
                    case 2: show5 = "Feby"; break;
                    case 3: show5 = "Mar"; break;
                    case 4: show5 = "Apr"; break;
                    case 5: show5 = "May"; break;
                    case 6: show5 = "Jun"; break;
                    case 7: show5 = "Jul"; break;
                    case 8: show5 = "Aug"; break;
                    case 9: show5 = "Sep"; break;
                    case 10: show5 = "Oct"; break;
                    case 11: show5 = "Nov"; break;
                    case 12: show5 = "Dec"; break;
                    default: show5="";
                }
                show5+=" "+day+" "+year;
                System.out.println(show5); 
                break;
            default: 
                System.out.println("Not have number of format!");
        }
    }
    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
}
