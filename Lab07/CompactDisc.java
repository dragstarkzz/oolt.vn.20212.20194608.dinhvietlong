/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.hedspi.aims.Aims.Media;

import java.util.ArrayList;

/**
 *
 * @author APC
 */
public class CompactDisc extends Disc implements Playable{
    private String artist;
    private ArrayList<Track> tracks = new ArrayList<>();

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public ArrayList<Track> getTracks() {
        return tracks;
    }

    public void setTracks(ArrayList<Track> tracks) {
        this.tracks = tracks;
    }

    public int getLength() {
        int sumlength = 0;
        for(int i=0; i<tracks.size(); i++){
            sumlength += tracks.get(i).getLength();
        }
        super.length = sumlength;
        return super.length;
    }

    public void addTrack(Track track){
        if(!tracks.contains(track)) 
            tracks.add(track);
        else System.out.println("Already exist");
    }
    public void removeTrack(Track track){
        if(tracks.contains(track)) 
            tracks.remove(track);
        else System.out.println("No have this track");
    }

    @Override
    public void play() {
        for(int i = 0; i<tracks.size(); i++){
            tracks.get(i).play();
        }
    } 
}
