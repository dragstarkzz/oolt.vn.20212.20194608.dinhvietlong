/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.hedspi.aims.Aims.Media;

/**
 *
 * @author APC
 */
public interface Playable {
        //đặc điểm của lớp interface là chỉ khai báo không có phần thân, phần thân để override
       public void play();
}