/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.hedspi.aims.Aims.Media;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author APC
 */
public class Book extends Media{
    private List<String> authors = new ArrayList<String>();

    public Book(String title) {
        super(title);
    }

    public Book(String title, String category) {
        super(title, category);
    }
    public Book(String title,
        String category,
        List<String> authors){
        super(title, category);
        this.authors = authors;
        //TODO: check author condition
    }
    public void addAuthor(String authorName){
        if(!authors.contains(authorName)) 
            authors.add(authorName);
    }
    public void removeAuthor(String authorName){
        if(authors.contains(authorName)) 
            authors.remove(authorName);
    }
}
