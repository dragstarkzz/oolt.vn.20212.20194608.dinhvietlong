/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.hedspi.aims.Aims.Media;

public class DigitalVideoDisc extends Disc implements Playable{

    public DigitalVideoDisc() {
    }

    public DigitalVideoDisc(int length, String director) {
        super(length, director);
    }

    public DigitalVideoDisc(int length, String director, String title, String category, float cost, int id) {
        super(length, director, title, category, cost, id);
    }

    public DigitalVideoDisc(String oldTitle) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public void printInfo(){
        System.out.println("-------------DVD Info------------");
        System.out.println("Title: "+getTitle());
        System.out.println("Category: "+getCategory());
        System.out.println("Editor: "+getDirector());
        System.out.println("Length: "+getLength()+" minutes");
        System.out.println("Cost: "+getCost()+"$");
    }
    public boolean searh(String title){
        String[] token = title.split(" ");
        boolean flag = true;
        for (String inx: token){
            if(!this.title.contains(inx)) flag = false;
        }
        return flag;
    }
    /*2.cac phuong thuc khoi tao (constructor)
    - Nhiem cu cau constructor: tao vung nho (memory)
    Chua thong tin cua object va thiet lap/ gan gia tri cho cac thuoc tinh cau object
    -Dac diem cuar constructor:
    +Ten cua constructor trung voi ten lop
    +Khong co kieu tra ve. khong dung tu khoa void
    -Co the xay dung nhieu constructor voi tham so khac nhau --> giup khoi tao doi tuong theo nhieu cach

    2.1 constructor khong tham so
    public DigitalVideoDisc(){
        this.title = "";
        this.category = "";
        this.editor = "";
        this.length = 0;
        this.cost = 0.0f;
    }
    2.2 constructor co 1 tham so: creat DVD object by title
    public DigitalVideoDisc(string title){
        this.title = title;
    }
    */

    @Override
    public void play() {
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
    }
}
