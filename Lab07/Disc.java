/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.hedspi.aims.Aims.Media;

/**
 *
 * @author APC
 */
public class Disc extends Media{
    int length;
    String director;

    public Disc() {
    }
    public Disc(int length, String director) {
        this.length = length;
        this.director = director;
    }
    public Disc(int length, String director, String title, String category, float cost, int id) {
        super(title, category, cost, id);
        this.length = length;
        this.director = director;
    }
    
    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }
}
