/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.hedspi.aims.Aims;

import hust.soict.hedspi.aims.order.Order.Order;
import hust.soict.hedspi.aims.disc.DigitalVideoDisc.DigitalVideoDisc;

/**
 *
 * @author APC
 */
public class Aims {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King"),
                         dvd2 = new DigitalVideoDisc("Robin Robin"),
                         dvd3 = new DigitalVideoDisc("Star Wars");
        dvd1.setCategory("Animation");
        dvd2.setCategory("Animation");
        dvd3.setCategory("Science Fiction");
        
        dvd1.setEditor("Rogger Allers");
        dvd2.setEditor("Daniel Ojari");
        dvd3.setEditor("George Lucas");
        
        dvd1.setLength(87);
        dvd2.setLength(30);
        dvd3.setLength(124);
        
        dvd1.setCost(19.95f);
        dvd2.setCost(7.5f);
        dvd3.setCost(24.95f);
        
        Order anOrder = new Order();
        anOrder.addDigitalVideoDisc(dvd1);
        anOrder.addDigitalVideoDisc(dvd2);
        anOrder.addDigitalVideoDisc(dvd3);
        anOrder.showListOrdered();
        System.out.println(dvd1.searh("The King"));
        //anOrder.removeDigitalVideoDisc(dvd1);
    }
}
