/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.hedspi.test.utils.DateTest;

import hust.soict.hedspi.aims.utils.MyDate.MyDate;

/**
 *
 * @author APC
 */
public class DateTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MyDate  date1 = new MyDate(),
                date2 = new MyDate("25th","October","1856"),
                date3 = new MyDate();
        date3.accept();
        date1.setDay(19);
        date1.setMonth(8);
        date1.setYear(1934);
        date1.print();
        date2.print();
        date3.print();

    }
    
}
