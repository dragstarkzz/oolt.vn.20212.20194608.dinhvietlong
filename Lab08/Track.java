/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.hedspi.aims.Aims.Media;

/**
 *
 * @author APC
 */
public class Track implements Playable, Comparable{
    private String title;
    private int length;

    public Track() {
    }
    
    public Track(String title, int length) {
        this.title = title;
        this.length = length;
    }
    
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
    @Override
    public void play() {
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
    }
    @Override
    public boolean equals(Object obj){
        Track track2 = (Track) obj;
        return (this.title.equals(track2.title) && this.length==track2.length);
    }
    @Override
    public int compareTo(Object obj){
        return this.title.compareTo(((Track) obj).getTitle());
    }
}
