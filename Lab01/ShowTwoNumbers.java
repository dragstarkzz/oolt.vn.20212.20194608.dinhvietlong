/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helloworld;

import javax.swing.JOptionPane;

/**
 *
 * @author APC
 */
public class ex4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String strNum1,strNum2;
        String strNotification = "You've just entered : ";
        strNum1 = JOptionPane.showInputDialog(null,"Please input first number: ","Input the first number",JOptionPane.INFORMATION_MESSAGE);
        strNotification += strNum1 + " and ";
        strNum2 = JOptionPane.showInputDialog(null,"Please input second number: ","Input the second number",JOptionPane.INFORMATION_MESSAGE);
        strNotification += strNum2;
        JOptionPane.showMessageDialog(null,strNotification,"Show two numbers", JOptionPane.INFORMATION_MESSAGE);
    }
    
}
