/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helloworld;

import static java.lang.Integer.parseInt;
import static java.time.Clock.system;
import javax.swing.JOptionPane;

/**
 *
 * @author APC
 */
public class ex6 {
    
    public static void FirstDegree(){
        String strNum1,strNum2;
        Double a, b,result;
        strNum1 = JOptionPane.showInputDialog(null,"ax+b=0\nPlease input a: ","solve first-degree",JOptionPane.INFORMATION_MESSAGE);
        strNum2 = JOptionPane.showInputDialog(null,"ax+b=0\nPlease input b: ","solve first-degree",JOptionPane.INFORMATION_MESSAGE);
        a = Double.parseDouble(strNum1);
        b = Double.parseDouble(strNum2);
        if(a==0 && b==0){
            result = 1.0;
        }else if (a==0 && b!=0){
            result = 2.0;
        }
        else result = -b/a;
        if(result==1.0){
            JOptionPane.showMessageDialog(null,a+"x+"+b+"=0\ninfinitely many solutions!","solve first-degree",JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
        }
        if(result==2.0){
            JOptionPane.showMessageDialog(null,a+"x+"+b+"=0\nno solution!","solve first-degree",JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
        }
        JOptionPane.showMessageDialog(null,a+"x+"+b+"=0\none experience = "+result, "solve first-degree",JOptionPane.INFORMATION_MESSAGE);
    }
    public static void SystemOfFirstDegree(){
        String strNum1,strNum2,strNum3,strNum4,strNum5,strNum6;
        Double a11,a12,a21,a22,b1,b2;
        Double D,D1,D2;
        Double[] result = {0.0, 0.0};
        strNum1 = JOptionPane.showInputDialog(null,"a11x+a12y=b1\na21x+a22y=b2\nPlease input a11: ","solve system of first-degree",JOptionPane.INFORMATION_MESSAGE);
        strNum2 = JOptionPane.showInputDialog(null,"a11x+a12y=b1\na21x+a22y=b2\nPlease input a12: ","solve system of first-degree",JOptionPane.INFORMATION_MESSAGE);
        strNum3 = JOptionPane.showInputDialog(null,"a11x+a12y=b1\na21x+a22y=b2\nPlease input a21: ","solve system of first-degree",JOptionPane.INFORMATION_MESSAGE);
        strNum4 = JOptionPane.showInputDialog(null,"a11x+a12y=b1\na21x+a22y=b2\nPlease input a22: ","solve system of first-degree",JOptionPane.INFORMATION_MESSAGE);
        strNum5 = JOptionPane.showInputDialog(null,"a11x+a12y=b1\na21x+a22y=b2\nPlease input b1: ","solve system of first-degree",JOptionPane.INFORMATION_MESSAGE);
        strNum6 = JOptionPane.showInputDialog(null,"a11x+a12y=b1\na21x+a22y=b2\nPlease input a2: ","solve system of first-degree",JOptionPane.INFORMATION_MESSAGE);
        a11 = Double.parseDouble(strNum1);
        a12 = Double.parseDouble(strNum2);
        a21 = Double.parseDouble(strNum3);
        a22 = Double.parseDouble(strNum4);
        b1 = Double.parseDouble(strNum5);
        b2 = Double.parseDouble(strNum6);
        D = a11*a22 - a21*a12;
        D1 = b1*a22 - b2*a12;
        D2 = a11*b2 - a21*b1;
        if(D!=0){
            result[0]=D1/D;
            result[1]=D2/D;
        }else if(D==0 && D1==0 && D2==0) result[0]=1.0;
        if(result[0]==1.0){
            JOptionPane.showMessageDialog(null,a11+"x1+"+a12+"x2="+b1+"\n"+a21+"x1+"+a22+"x2="+b2+"\ninfinitely many solutions!","solve system of first-degree",JOptionPane.INFORMATION_MESSAGE);
        }else if(result[0]==0.0){
            JOptionPane.showMessageDialog(null,a11+"x1+"+a12+"x2="+b1+"\n"+a21+"x1+"+a22+"x2="+b2+"\nno solution!","solve system of first-degree",JOptionPane.INFORMATION_MESSAGE);
        }else JOptionPane.showMessageDialog(null,a11+"x1+"+a12+"x2="+b1+"\n"+a21+"x1+"+a22+"x2="+b2+"\nhas one experience (x1,x2)="+"("+String.format("%.2f",result[0])+","+String.format("%.2f",result[1])+")","solve system of first-degree",JOptionPane.INFORMATION_MESSAGE);
    }
    public static void SecondDegree(){
        String strNum1,strNum2,strNum3;
        Double a,b,c;
        strNum1 = JOptionPane.showInputDialog(null,"ax^2+bx+c=0\nPlease input a: ","solve second-degree",JOptionPane.INFORMATION_MESSAGE);
        strNum2 = JOptionPane.showInputDialog(null,"ax^2+bx+c=0\nPlease input b: ","solve second-degree",JOptionPane.INFORMATION_MESSAGE);
        strNum3 = JOptionPane.showInputDialog(null,"ax^2+bx+c=0\nPlease input c: ","solve second-degree",JOptionPane.INFORMATION_MESSAGE);
        a = Double.parseDouble(strNum1);
        b = Double.parseDouble(strNum2);
        c = Double.parseDouble(strNum3);
        if(a == 0){
            Double result;
            if(b==0 && c==0){
                result = 1.0;
            }else if (b==0 && c!=0){
                result = 2.0;
            }
            else result = -c/b;
            if(result==1.0){
                JOptionPane.showMessageDialog(null,a+"x^2+"+b+"x+"+c+"=0\ninfinitely many solutions!","solve second-degree",JOptionPane.INFORMATION_MESSAGE);
                System.exit(0);
            }
            if(result==2.0){
                JOptionPane.showMessageDialog(null,a+"x^2+"+b+"x+"+c+"=0\nno solution!","solve second-degree",JOptionPane.INFORMATION_MESSAGE);
                System.exit(0);
            }
            JOptionPane.showMessageDialog(null,a+"x^2+"+b+"x+"+c+"=0\none experience = "+result, "solve second-degree",JOptionPane.INFORMATION_MESSAGE);
        }
        if(a!=0){
            Double delta = b*b-4*a*c;
            if(delta<0){
                JOptionPane.showMessageDialog(null,a+"x^2+"+b+"x+"+c+"=0\nno solution!","solve second-degree",JOptionPane.INFORMATION_MESSAGE);
                System.exit(0);
            }else if(delta == 0){
                JOptionPane.showMessageDialog(null,a+"x^2+"+b+"x+"+c+"=0\nhas one experience x = "+(-b/2*a),"solve second-degree",JOptionPane.INFORMATION_MESSAGE);
                System.exit(0);
            }else JOptionPane.showMessageDialog(null,a+"x^2+"+b+"x+"+c+"=0\nhas two experiences x1 = "+((-b+Math.sqrt(delta))/2*a)+"\nx2 = "+((-b-Math.sqrt(delta))/2*a),"solve second-degree",JOptionPane.INFORMATION_MESSAGE);
        }
    }
    public static void main(String[] args) {
        String temp;
        int input = 0;
        while(input!=4){
            temp = JOptionPane.showInputDialog(null,"1.solve for first-degree\n2.solve for system of first-degree\n3.solve for second-degree\n4.exit\n", "chosen one: ",JOptionPane.INFORMATION_MESSAGE);
            input = parseInt(temp);
            if(input==1)FirstDegree();
            if(input==2)SystemOfFirstDegree(); 
            if(input==3)SecondDegree();
        }
    }    
}
