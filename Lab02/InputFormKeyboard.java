/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week2;

import java.util.Scanner;

/**
 *
 * @author APC
 */
public class InputFormKeyboard {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("What your name?");
        String strName = keyboard.nextLine();
        System.out.println("How old are you ?");
        int iAge = keyboard.nextInt();
        System.out.println("How tall are you (m) ?");
        Double iHeight = keyboard.nextDouble();
        System.out.println("Mr/Mrs "+strName+", "+iAge+" years old. Your Height is "+iHeight+" m.");
    }
    
}
