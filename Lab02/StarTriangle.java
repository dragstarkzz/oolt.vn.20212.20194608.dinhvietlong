/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week2;

import java.util.Scanner;

/**
 *
 * @author APC
 */
public class StarTriangle {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Please enter the height of triangle: ");
        int n = keyboard.nextInt();
        int max = n;
        if(n<0) System.out.println("height is not negetive number ");
        else{
            for(int i=1; i <= max; i++){
                for(int j = n; j > 0; j--){
                    System.out.print(" ");
                }
                n--;
                for(int j = 0; j < i+i-1 ; j++){
                System.out.print("*");
            }
            System.out.println("");
        }
        }
        
    }
    
}
