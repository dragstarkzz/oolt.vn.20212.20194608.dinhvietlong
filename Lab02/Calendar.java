/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week2;

import static java.lang.Integer.parseInt;
import java.util.Scanner;

/**
 *
 * @author APC
 */
public class Calendar {

    public static int converseMonth(String month){
        int num = 0;
        if(month.length()<=2){
            num = parseInt(month);
            return num;
        }
        if( month.compareTo("January") == 0 || 
            month.compareTo("Jan") == 0 ||
            month.compareTo("Jan.") == 0 ){
            num = 1;
        }
        if( month.compareTo("February") == 0 || 
            month.compareTo("Feb") == 0 ||
            month.compareTo("Feb.") == 0 ){
            num = 2;
        }
        if( month.compareTo("March") == 0 || 
            month.compareTo("Mar") == 0 ||
            month.compareTo("Mar.") == 0 ){
            num = 3;
        }
        if( month.compareTo("April") == 0 || 
            month.compareTo("Apr") == 0 ||
            month.compareTo("Apr.") == 0 ){
            num = 4;
        }
        if( month.compareTo("May") == 0 ){
            num = 5;
        }
        if( month.compareTo("June") == 0 || 
            month.compareTo("Jun") == 0 ){
            num = 6;
        }
        if( month.compareTo("July") == 0 || 
            month.compareTo("Jul") == 0){
            num = 7;
        }
        if( month.compareTo("August") == 0 || 
            month.compareTo("Aug") == 0 ||
            month.compareTo("Aug.") == 0 ){
            num = 8;
        }
        if( month.compareTo("September") == 0 || 
            month.compareTo("Sep") == 0 ||
            month.compareTo("Sep.") == 0 ){
            num = 9;
        }
        if( month.compareTo("October") == 0 || 
            month.compareTo("Oct") == 0 ||
            month.compareTo("Oct.") == 0 ){
            num = 10;
        }
        if( month.compareTo("November") == 0 || 
            month.compareTo("Nov") == 0 ||
            month.compareTo("Nov.") == 0 ){
            num = 11;
        }
        if( month.compareTo("December") == 0 || 
            month.compareTo("Dec") == 0 ||
            month.compareTo("Dec.") == 0 ){
            num = 12;
        }
        return num;
    }
    public static Boolean checkYear(int year){
        return (((year % 4 == 0) && (year % 100 != 0)) ||
             (year % 400 == 0));
    }
    public static int showDay(int month, int year){
        switch(month){
            case 1: return 31;
            case 2: 
                if(checkYear(year)) return 29;
                else return 28;
            case 3: return 31;
            case 4: return 30;
            case 5: return 31;
            case 6: return 30;
            case 7: return 31;
            case 8: return 31;
            case 9: return 30;
            case 10: return 31;
            case 11: return 30;
            case 12: return 31;
        }
        return 0;
    }
    
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter month: ");
        String strMonth = keyboard.nextLine();
        int intMonth = converseMonth(strMonth);
        System.out.println("Enter year: ");
        String strYear = keyboard.nextLine();
        int intYear = parseInt(strYear);
        
        System.out.println(strMonth+" "+strYear+" has "+showDay(intMonth,intYear)+" days.");
    }
    
}
