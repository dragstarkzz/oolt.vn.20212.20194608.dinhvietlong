/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week2;

import java.util.Scanner;

/**
 *
 * @author APC
 */
public class AddTwoMatrices {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int r1,c1,r2,c2;
        Scanner inp = new Scanner(System.in);
        System.out.println("Nhập kích thước ma trận 1 M[r1][c1]: ");
        System.out.println("r1: ");
        r1 = inp.nextInt();
        System.out.println("c1: ");
        c1 = inp.nextInt();
        System.out.println("Nhập kích thước ma trận 2 N[r2][c2]: ");
        System.out.println("r2: ");
        r2 = inp.nextInt();
        System.out.println("c2: ");
        c2 = inp.nextInt();
        if(r1!=r2 && c1!=c2) 
        { 
            System.out.println("\nHai ma trận không cùng kích thước, không thể cộng."); 
            System.exit(0);
        } 
        System.out.println("Nhập các phần từ của ma trận M: ");
        int a[][] = new int[r1][c1];
        for(int i=0;i<r1;i++)
        {
            for(int j=0;j<c1;j++)
            {
                a[i][j]=inp.nextInt();
            }
        }
        System.out.println("Nhập các phần tử của ma trận N: ");
        int b[][] = new int[r2][c2];
        for(int i=0;i<r2;i++)
        {
            for(int j=0;j<c2;j++)
            {
                b[i][j]=inp.nextInt();
            }
        }
        
        int c[][] = new int[r1][c1];
        for(int i=0;i<r1;i++)
        {
            for(int j=0;j<c1;j++)
            {
                c[i][j]=a[i][j]+b[i][j];
                System.out.print(c[i][j] + " ");
            }
            System.out.println();
        }
    }  
}
