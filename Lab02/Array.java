/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week2;

import java.util.Arrays;

/**
 *
 * @author APC
 */
public class Array {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] arr = { 13, 7, 6, 45, 21, 9, 101, 102 };
        System.out.println("Original arr[] : "+Arrays.toString(arr));
        Arrays.sort(arr);
        int sum = 0, average;
        for(int i=0; i< arr.length; i++){
            sum += arr[i] ;
        }
        average = sum/arr.length;
        System.out.println("Modified arr[] : "+Arrays.toString(arr));
        System.out.println("Sum of all elements : "+sum);
        System.out.println("Average of all elements : "+average);
    }
}
