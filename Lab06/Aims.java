/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.hedspi.aims.Aims;

import hust.soict.hedspi.aims.order.Order.Order;
import hust.soict.hedspi.aims.Aims.Media.Media;
import java.util.Scanner;

/**
 *
 * @author APC
 */
public class Aims {
    public static void showMenu() {
            System.out.println("Order Management Application: ");
            System.out.println("--------------------------------");
            System.out.println("1. Create new order");
            System.out.println("2. Add item to the order");
            System.out.println("3. Delete item by id");
            System.out.println("4. Display the items list of order");
            System.out.println("0. Exit");
            System.out.println("--------------------------------");
            System.out.println("Please choose a number(0-1-2-3-4):");
    }
    public static void main(String[] args) {
        int select;
        Thread memCon = new Thread((Runnable) new MemoryDaemon(), "Memory Observation");
        memCon.setDaemon(true);
        memCon.start();
        int orderId = -1;
        int orderAddId;
        int orderDelId;
        int deleteId;
        int itemId;
        int showId;
        Media toAdd = new Media();
        Scanner keyboard = new Scanner(System.in);
        Order[] OrderList = new Order[Order.MAX_LIMITTED_ORDER];
        do {
            showMenu();
            select = keyboard.nextInt();
            switch(select){
                case 1: orderId++;
                        OrderList[orderId] = new Order();
                        OrderList[orderId].getInstanceOrder(orderId);
                        break;
                case 2: System.out.println("input id of order list");
                        orderAddId = keyboard.nextInt();
                        itemId = OrderList[orderAddId].itemsOrdered.size();
                        toAdd.setId(itemId);
                        System.out.print("input title: ");
                        toAdd.setTitle(keyboard.next());
                        System.out.print("input category: ");
                        toAdd.setCategory(keyboard.next());
                        System.out.print("input cost: ");
                        toAdd.setCost(keyboard.nextFloat());
                        OrderList[orderId].addMedia(toAdd);
                        break;
                case 3: System.out.println("input id of order list");
                        orderDelId = keyboard.nextInt();
                        System.out.print("input id to delete: ");
                        deleteId = keyboard.nextInt();
                        OrderList[orderDelId].removeMedia(deleteId);
                        break;
                case 4: System.out.print("input id to show order list: ");
                        showId = keyboard.nextInt();
                        OrderList[showId].showListOrdered();
                        break;
                case 0: break;
                default: System.out.println("Please choose a number(0-1-2-3-4):"); 
                         break;
            }
        }while(select!=0);
    }
}
