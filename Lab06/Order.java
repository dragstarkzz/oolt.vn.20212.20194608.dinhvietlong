/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.hedspi.aims.order.Order;

import hust.soict.hedspi.aims.utils.MyDate.MyDate;
import hust.soict.hedspi.aims.Aims.Media.Media;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Order {
    public static final int MAX_NUMBERS_ORDER = 5; //so luong item nhieu nhat co the co
    public static final int MAX_LIMITTED_ORDER = 5; //so luong order nhieu nhat co the order
    public int id; 
    public ArrayList<Media> itemsOrdered = new ArrayList<Media>();
    private MyDate dateOrdered = new MyDate("21th","January","1"); //instance variable
    private int nbOrders = 0;

    public Order() {
    }

    public Order(int id) {
        this.id = id;
    }
    
    public void getInstanceOrder(int id_in){
        this.id = id_in;
        LocalDateTime dateObj = LocalDateTime.now();
        dateOrdered.day = dateObj.getDayOfMonth();
        dateOrdered.month = dateObj.getMonthValue();
        dateOrdered.year = dateObj.getYear();
 
        if(nbOrders > MAX_LIMITTED_ORDER) System.out.println("Over limit order!");
    }
    private boolean checkLimit(int num){
        if(num >= MAX_NUMBERS_ORDER){
            System.out.println("the dvd(s) or book(s) that could not be added, e.g., the item quantity has reached its limit!");
            return true;
        }
        return false;
    }
    public void addMedia(Media toAdd){
        if(checkLimit(itemsOrdered.size())) return;
        itemsOrdered.add(toAdd);
    }
    public void removeMedia(String title){
        for(int i=0; i<itemsOrdered.size(); i++){
            if( itemsOrdered.get(i).getTitle() == title){
                itemsOrdered.remove(i);
                return;
            }
        }
    }
    public void removeMedia(int id){
        for(int i=0; i<itemsOrdered.size(); i++){
            if(itemsOrdered.get(i).getId() == id){
                itemsOrdered.remove(i);
                return;
            }
        }
    }
    public float totalCost(){
        float total = 0.0f;
        for(int i = 0; i < itemsOrdered.size(); i++){
            total+=this.itemsOrdered.get(i).getCost();
        }
        return total;
    }
    public void showListOrdered(){
        System.out.println("***********************Order***********************");
        System.out.print("Date: ");
        dateOrdered.printByFormat(5);
        System.out.println("Ordered Items:");
        for(int i = 0; i < itemsOrdered.size(); i++){
            System.out.println((i+1)+". "+this.itemsOrdered.get(i).getTitle()+" - "
                    +this.itemsOrdered.get(i).getCategory()+" - "
                    +this.itemsOrdered.get(i).getCost()+"$");
        }
        System.out.println("Total cost: "+totalCost()+"$");
        Media toFree;
        toFree = this.getALuckyItem();
        System.out.println("Free Item: "+toFree.getTitle());
        System.out.println("Final cost: "+(totalCost()-toFree.getCost())+"$");
        System.out.println("***************************************************");
    }
    public Media getALuckyItem(){
        int lucky = (int) ((Math.random()*100000)%itemsOrdered.size());
        return itemsOrdered.get(lucky);
    }
    
}
