/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.hedspi.aims.Aims.Media;

public class DigitalVideoDisc extends Media{
    private String editor;
    private int length;
    public DigitalVideoDisc(){
        title = "";
        category = "";
        editor = "";
        length = 0;
        cost = 0.0f;
    }
    public DigitalVideoDisc(String title){
        title = title;
    }

    public DigitalVideoDisc(String editor, int length) {
        this.editor = editor;
        this.length = length;
    }

    public DigitalVideoDisc(String editor, int length, String title) {
        super(title);
        this.editor = editor;
        this.length = length;
    }

    public DigitalVideoDisc(String editor, int length, String title, String category) {
        super(title, category);
        this.editor = editor;
        this.length = length;
    }

    public DigitalVideoDisc(String editor, int length, String title, String category, float cost, int id) {
        super(title, category, cost, id);
        this.editor = editor;
        this.length = length;
    }
    
    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        if(length >=0)
            this.length = length;
        else 
            this.length = 0;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        if(cost >= 0)
            this.cost = cost;
        else 
            this.cost = 0.0f;
    }
    public void printInfo(){
        System.out.println("-------------DVD Info------------");
        System.out.println("Title: "+getTitle());
        System.out.println("Category: "+getCategory());
        System.out.println("Editor: "+getEditor());
        System.out.println("Length: "+getLength()+" minutes");
        System.out.println("Cost: "+getCost()+"$");
    }
    public boolean searh(String title){
        String[] token = title.split(" ");
        boolean flag = true;
        for (String inx: token){
            if(!this.title.contains(inx)) flag = false;
        }
        return flag;
    }
    /*2.cac phuong thuc khoi tao (constructor)
    - Nhiem cu cau constructor: tao vung nho (memory)
    Chua thong tin cua object va thiet lap/ gan gia tri cho cac thuoc tinh cau object
    -Dac diem cuar constructor:
    +Ten cua constructor trung voi ten lop
    +Khong co kieu tra ve. khong dung tu khoa void
    -Co the xay dung nhieu constructor voi tham so khac nhau --> giup khoi tao doi tuong theo nhieu cach

    2.1 constructor khong tham so
    public DigitalVideoDisc(){
        this.title = "";
        this.category = "";
        this.editor = "";
        this.length = 0;
        this.cost = 0.0f;
    }
    2.2 constructor co 1 tham so: creat DVD object by title
    public DigitalVideoDisc(string title){
        this.title = title;
    }
    */
}
