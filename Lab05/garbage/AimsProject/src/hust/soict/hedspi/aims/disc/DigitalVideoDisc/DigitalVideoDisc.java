/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.hedspi.aims.disc.DigitalVideoDisc;

public class DigitalVideoDisc {
    private String title;
    private String category;
    private String editor;
    private int length;
    private float cost;
    public DigitalVideoDisc(String title){
        this.title = title;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }
    public boolean searh(String title){
        String[] token = title.split(" ");
        boolean flag = true;
        for (String inx: token){
            if(!this.title.contains(inx)) flag = false;
        }
        return flag;
    }
}
