/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.hedspi.aims.order.Order;

import hust.soict.hedspi.aims.utils.MyDate.MyDate;
import hust.soict.hedspi.aims.disc.DigitalVideoDisc.DigitalVideoDisc;
import java.time.LocalDateTime;

public class Order {
    public static final int MAX_NUMBERS_ORDER = 5;
    public static final int MAX_LIMITTED_ORDER = 5;
    public int qtyOrdered = 0; //instance variable
    private DigitalVideoDisc[] itemOrdered = new DigitalVideoDisc[MAX_NUMBERS_ORDER]; //instance variable
    private MyDate dateOrdered = new MyDate("21th","January","1"); //instance variable
    private static int nbOrders = 0;
    public Order(){
        LocalDateTime dateObj = LocalDateTime.now();
        dateOrdered.day = dateObj.getDayOfMonth();
        dateOrdered.month = dateObj.getMonthValue();
        dateOrdered.year = dateObj.getYear();
        nbOrders++;
        if(nbOrders > MAX_LIMITTED_ORDER) System.out.println("Over limit order!");
    }
    private boolean checkLimit(int num){
        if(num >= MAX_NUMBERS_ORDER){
            System.out.println("the dvd(s) that could not be added, e.g., the item quantity has reached its limit!");
            return false;
        }
        return true;
    }
    public void addDigitalVideoDisc(DigitalVideoDisc disc){
        if(checkLimit(qtyOrdered)) this.itemOrdered[this.qtyOrdered++] = disc;
    }
    public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList){
        if(checkLimit(qtyOrdered+dvdList.length)){
            for (DigitalVideoDisc index : dvdList){
                this.itemOrdered[this.qtyOrdered++] = index;
            }
        }
    }
    public void removeDigitalVideoDisc(DigitalVideoDisc disc){
        for(int i = 0; i < qtyOrdered; i++){
            if(disc.getTitle().compareTo(this.itemOrdered[i].getTitle())==0){
                for (int j = i; j < qtyOrdered; j++){
                   this.itemOrdered[j] = this.itemOrdered[j+1];
                }
                this.qtyOrdered--;
            }
        }
    }
    public float totalCost(){
        float total = 0.0f;
        for(int i = 0; i < qtyOrdered; i++){
            total+=this.itemOrdered[i].getCost();
        }
        return total;
    }
    public void showListOrdered(){
        System.out.println("***********************Order***********************");
        System.out.print("Date: ");
        dateOrdered.printByFormat(5);
        System.out.println("Ordered Items:");
        for(int i = 0; i < qtyOrdered; i++){
            System.out.println((i+1)+". DVD - "+this.itemOrdered[i].getTitle()+" - "
                    +this.itemOrdered[i].getCategory()+" - "
                    +this.itemOrdered[i].getEditor()+" - "
                    +this.itemOrdered[i].getLength()+" minutes : "
                    +this.itemOrdered[i].getCost()+"$");
        } 
        System.out.println("Total cost: "+totalCost()+"$");
	DigitalVideoDisc toFree = new DigitalVideoDisc();
        toFree = this.getALuckyItem();
        System.out.println("Free Item: "+toFree.getTitle());
        System.out.println("Final cost: "+(totalCost()-toFree.getCost())+"$");
        System.out.println("***************************************************");
    }
    public DigitalVideoDisc getALuckyItem(){
        int lucky = (int) ((Math.random()*100000)%qtyOrdered);
        return itemOrdered[lucky];
    }
    
}
