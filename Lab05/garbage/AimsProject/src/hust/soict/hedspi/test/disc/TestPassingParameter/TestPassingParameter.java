/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.globalict.test.disc.TestPassingParameter;

import hust.soict.hedspi.aims.disc.DigitalVideoDisc.DigitalVideoDisc;

/**
 *
 * @author APC
 */
public class TestPassingParameter {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle"),
                         cinderellaDVD = new DigitalVideoDisc("Cinderella");
        swap(jungleDVD,cinderellaDVD);
        System.out.println("jungle dvd title is: "+jungleDVD.getTitle());
        System.out.println("cinderella dvd tilte is: "+cinderellaDVD.getTitle());
        
        changeTitle(jungleDVD,cinderellaDVD.getTitle());
        System.out.println("jungle dvd title is: "+jungleDVD.getTitle());
    }
    public static void swap(Object o1, Object o2){
        Object tmp = o1;
        o1 = o2;
        o2 = tmp;
    }
    public static void changeTitle(DigitalVideoDisc dvd, String title){
        String oldTitle = dvd.getTitle();
        dvd.setTitle(title);
        dvd = new DigitalVideoDisc(oldTitle);
    }
    
}
